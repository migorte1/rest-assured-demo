package atd.rest.assured.demo;

import net.serenitybdd.core.Serenity;

public class SessionController {

    public static <T> T popSessionVariable(String variableName) {
        T popedVariable = Serenity.sessionVariableCalled(variableName);
        Serenity.setSessionVariable(variableName).to(null);
        return popedVariable;
    }
}
