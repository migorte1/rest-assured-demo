package atd.rest.assured.demo;

public class SessionVariables {
    public static final String ASSET_SESSION_VARIABLE = "asset";
    public static final String ASSET_PARTS_SESSION_VARIABLE = "assetParts";
    public static final String AUTH_TOKEN_SESSION_VARIABLE = "authToken";
    public static final String BODY_SESSION_VARIABLE = "body";
    public static final String CONTENT_TYPE_SESSION_VARIABLE = "contentType";
    public static final String EXPECTED_VALUES_SESSION_VARIABLE = "expectedValuesParameters";
    public static final String HEADER_PARAMS_SESSION_VARIABLE = "headerParams";
    public static final String MULTIPART_FILE_SESSION_VARIABLE = "multipartFile";
    public static final String PATH_PARAMETERS_SESSION_VARIABLE = "pathParameters";
    public static final String QUERY_PARAMETERS_SESSION_VARIABLE = "queryParameters";
    public static final String RESPONSE_SESSION_VARIABLE = "response";
    public static final String STATUS_CODE_SESSION_VARIABLE = "statusCode";
}
