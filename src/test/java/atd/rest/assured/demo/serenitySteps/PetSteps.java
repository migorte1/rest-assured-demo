package atd.rest.assured.demo.serenitySteps;

import atd.rest.assured.demo.RegexMatcher;
import atd.rest.assured.demo.ServicesConfiguration;
import atd.rest.assured.demo.SessionController;
import com.google.gson.JsonParser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.rest.RestMethod;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

import static atd.rest.assured.demo.SessionVariables.*;
import static net.serenitybdd.rest.SerenityRest.rest;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class PetSteps {

    @Step("Send {0} to the endpoint {1}")
    public void sendRequest(RestMethod method, String endpoint) {
        RequestSpecification specification = rest()
                .baseUri(ServicesConfiguration.baseURI);

        Response response = buildSpecification(specification, method, endpoint)
                .then()
                .extract()
                .response();
        Serenity.setSessionVariable(RESPONSE_SESSION_VARIABLE).to(response);
    }

    @Step
    private Response buildSpecification(RequestSpecification specification, RestMethod method, String endpoint) {
        Map<String, ? super Object> headerParams = SessionController.popSessionVariable(HEADER_PARAMS_SESSION_VARIABLE);

        Map<String, ? super Object> pathParams = SessionController.popSessionVariable(PATH_PARAMETERS_SESSION_VARIABLE);

        Map<String, ? super Object> queryParams = SessionController.popSessionVariable(QUERY_PARAMETERS_SESSION_VARIABLE);

        String contentType = SessionController.popSessionVariable(CONTENT_TYPE_SESSION_VARIABLE);

        String body = SessionController.popSessionVariable(BODY_SESSION_VARIABLE);

        Integer statusCode = SessionController.popSessionVariable(STATUS_CODE_SESSION_VARIABLE);

        if (headerParams != null)
            specification.headers(headerParams);

        if (queryParams != null)
            specification.params(queryParams);

        if (pathParams == null)
            pathParams = new HashMap<>();

        if (contentType != null)
            specification.contentType(contentType);

        if (body != null)
            specification.body(body);

        Response response;

        switch (method) {
            case GET:
                response = specification
                        .when()
                        .get(endpoint, pathParams);
                break;
            case POST:
                response = specification
                        .when()
                        .post(endpoint, pathParams);
                break;
            case HEAD:
                response = specification
                        .when()
                        .head(endpoint, pathParams);
                break;
            case DELETE:
                response = specification
                        .when()
                        .delete(endpoint, pathParams);
                break;
            case PUT:
                response = specification
                        .when()
                        .put(endpoint, pathParams);
                break;
            default:
                throw new IllegalArgumentException("Method not supported");
        }
        if (statusCode != null) {
            response.then().statusCode(statusCode);
        }
        return response;
    }

    @Step("the response body should be {0}")
    public void verifyBody(String responseBody) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        JsonParser parser = new JsonParser();
        Assert.assertThat(
                "Unexpected JSON.",
                parser.parse(response.getBody().asString()),
                equalTo(parser.parse(responseBody))
        );
    }

    @Step
    public void verifyPathValue(String path, String value) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        response.then().body(path, RegexMatcher.matchesRegex(value));
    }

    @Step
    public void verifyStatus(int statusCode) {
        Response response = Serenity.sessionVariableCalled(RESPONSE_SESSION_VARIABLE);
        Assert.assertThat(
                "Status code must be: " + statusCode,
                response.getStatusCode(),
                is(statusCode)
        );

    }
}
