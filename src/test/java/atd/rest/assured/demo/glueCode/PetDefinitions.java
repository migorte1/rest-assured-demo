package atd.rest.assured.demo.glueCode;

import atd.rest.assured.demo.serenitySteps.PetSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.rest.RestMethod;
import net.thucydides.core.annotations.Steps;


import java.util.Map;

import static atd.rest.assured.demo.SessionVariables.BODY_SESSION_VARIABLE;
import static atd.rest.assured.demo.SessionVariables.HEADER_PARAMS_SESSION_VARIABLE;
import static atd.rest.assured.demo.SessionVariables.QUERY_PARAMETERS_SESSION_VARIABLE;

public class PetDefinitions {

    @Steps
    PetSteps steps;

    @Given("^the headers are:$")
    public void theHeadersAre(Map<String, String> headers){
        Serenity.setSessionVariable(HEADER_PARAMS_SESSION_VARIABLE).to(headers);
    }

    @Given("^the query parameters are:$")
    public void theQueryParametersAre(Map<String, String> queryParameters) throws Throwable {
        Serenity.setSessionVariable(QUERY_PARAMETERS_SESSION_VARIABLE).to(queryParameters);
    }

    @When("^I send a \"(.*)\" to \"(.*)\"$")
    public void iSendATo(RestMethod method, String endpoint) throws Throwable {
        steps.sendRequest(method, endpoint);
    }

    @Then("^the body shall be:$")
    public void theBodyShallBe(String body) throws Throwable {
        steps.verifyBody(body);
    }

    @And("^the body is:$")
    public void theBodyIs(String body) throws Throwable {
        Serenity.setSessionVariable(BODY_SESSION_VARIABLE).to(body);
    }

    @And("^the value for \"(.*)\" should be \"(.*)\"$")
    public void theValueForShouldBe(String path, String value) throws Throwable {
        steps.verifyPathValue(path, value);
    }

    @Then("^the response should return status code \"(\\d+)\"$")
    public void theResponseShouldReturnStatusCode(int statusCode) throws Throwable {
        steps.verifyStatus(statusCode);
    }
}
