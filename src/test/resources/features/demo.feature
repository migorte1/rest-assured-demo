Feature: Demo pet store

  Scenario: Login
    Given the headers are:
      | Content-Type | application/json |
      | Accept       | application/json |
    And the query parameters are:
      | username | adidas_user |
      | password | atd_2017    |
    When I send a "GET" to "/user/login"
    Then the response should return status code "200"

  Scenario Outline: Create new pet
    Given the headers are:
      | Content-Type | application/json |
      | Accept       | application/json |
    And the body is:
    """
    {
      "id": 0,
      "name": "<name>",
      "photoUrls": [
        "string"
      ],
      "tags": [
        null
      ],
      "status": "<status>"
    }
    """
    When I send a "POST" to "/pet"
    Then the value for "name" should be "<name>"
    And the value for "status" should be "<status>"
    Examples:
      | name | status    |
      | Toby | available |

  Scenario Outline: Get pet
    Given the headers are:
      | Content-Type | application/json |
      | Accept       | application/json |
    When I send a "GET" to "/pet/<petId>"
    Then the response should return status code "200"
    And the value for "name" should be "<name>"
    Examples:
      | petId | name  |
      | 1500  | Butch |

